# strict-emitter

Strictly typed EventEmitter for Typescript.

Everything is strictly typed, from event names to event signatures. The
declaration is provided through the `Events` type argument and should be an
interface containing the signatures of the events.

## Example

```ts
import { StrictEmitter } from 'strict-emitter';

/** Declaration of events for Chat. */
interface ChatEvents {
  /** Triggered when a new message is received. */
  message(from: string, message: string): void;
}

export class Chat extends StrictEmitter<ChatEvents> {
  receive(from: string, message: string) {
    this.emit('message', from, message);
  }
}
```

```ts
import { Chat } from 'chat';

const chat = new Chat();

chat.on('message', (from, message) => {
  console.log(`${from}: ${message}`);
});
```

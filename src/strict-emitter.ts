import { EventEmitter } from 'events';

/** Matches any function signature. */
type AnyFunction = (...args: any[]) => any;

/** A listener is a handler function */
type Listener<Events, Key extends EventKey<Events>, Event = Events[Key]> = (
  ...args: EventIn<Event>
) => EventOut<Event>;

/** The parameters of an event. */
type EventIn<Event> = Event extends AnyFunction ? Parameters<Event> | [] : [];

/** The return type of an event. */
type EventOut<Event> = Event extends AnyFunction
  ? ReturnType<Event> | void
  : [];

/** The keys of members matching a function signature in an events interface. */
type EventKey<Events, Keys extends keyof Events = keyof Events> = {
  [Key in Keys]: Events[Key] extends AnyFunction ? Key : never;
}[Keys] &
  (string | symbol);

/**
 * A strictly typed EventEmitter based class.
 *
 * StrictEmitter is effectively EventEmitter under the hood, but has strictly
 * typed events and methods.
 *
 * @template Events An interface representing the events declaration for the
 * emitter, in which each method is a signature for an event; e.g.
 * `{ click(e: ClickEvent): void; }`
 */
// @ts-ignore
export interface StrictEmitter<
  Events,
  Event extends EventKey<Events> = EventKey<Events>
> {
  /**
   * Calls the registered listeners for a event.
   *
   * @param event The event to call listeners for.
   * @param args The parameters to pass to the listeners.
   * @returns Whether any listeners have been called.
   */
  emit(event: Event, ...args: EventIn<Events[Event]>): boolean;

  /**
   * Adds a listener for an event.
   *
   * @param event The event to add a listener for.
   * @param listener The listener to add.
   */
  on(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Adds a listener for an event.
   *
   * @param event The event to add a listener for.
   * @param listener The listener to add.
   */
  addListener(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Adds a listener for an event to the beginning of the listeners array.
   *
   * Prepending the listener results in it being called prior to the previously
   * added listeners due to the synchronous nature of `emit()`.
   *
   * @param event The event to add a listener for.
   * @param listener The listener to prepend.
   */
  prependListener(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Adds a one-time listener for an event.
   *
   * A one-time listener is removed and invoked when the event is triggered.
   *
   * @param event The event to add a listener for.
   * @param listener The listener to add.
   */
  once(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Adds a one-time listener for an event to the beginning of the listeners
   * array.
   *
   * A one-time listener is removed and invoked when the event is triggered.
   *
   * Prepending the listener results in it being called prior to the previously
   * added listeners due to the synchronous nature of `emit()`.
   *
   * @param event The event to add a listener for.
   * @param listener The listener to prepend.
   */
  prependOnceListener(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Removes a listener for an event.
   *
   * @param event The event to remove a listener for.
   * @param listener The listener to remove.
   */
  off(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Removes a listener for an event.
   *
   * @param event The event to remove a listener for.
   * @param listener The listener to remove.
   */
  removeListener(event: Event, listener: Listener<Events, Event>): this;

  /**
   * Returns an array of listeners for an event.
   */
  listeners(event: Event): Array<Listener<Events, Event>>;

  /**
   * Returns an array of listeners for an event, including any wrappers.
   */
  rawListeners(event: Event): Array<Listener<Events, Event>>;

  /**
   * Removes all listeners or listeners for a specified event.
   *
   * @param event The event to remove listeners for.
   */
  removeAllListeners(event?: Event): this;

  /**
   * Sets the maximum amount of listeners for each event.
   *
   * @param n The maximum amount.
   */
  setMaxListeners(n: number): this;

  /**
   * Gets the maximum amount of listeners for each event.
   *
   * @returns The maximum amount.
   */
  getMaxListeners(): number;

  /**
   * Returns the number of listeners for an event.
   *
   * @param event The event to get the count for.
   * @returns The number of listeners.
   */
  listenerCount(event: Event): number;

  /**
   * Returns an array of events that have registered listeners.
   *
   * @returns The array of events.
   */
  eventNames(): Array<string | symbol>;
}

// Declare the StrictEmitter class.
// @ts-ignore
export class StrictEmitter<Events> extends EventEmitter {}

// Set the prototype of StrictEmitter to the prototype of EventEmitter, so
// StrictEmitter is effectively EventEmitter under the hood, albeit with
// different signatures.
// StrictEmitter.prototype = EventEmitter.prototype as any;
